
export class User {
    _id: string;
    roles: any;
    first_name: string;
    last_name: string;
    username: string;
    phone_number: number;
    email: string;
    country_name: string;
    city: string;
    latitude: string;
    longitude: string;
    avatar: any;
    password: string;
}