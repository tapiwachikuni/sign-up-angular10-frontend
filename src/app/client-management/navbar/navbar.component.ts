import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { TokenStorageService } from 'src/app/services/token-storage.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  public isLoggedIn = false;
  public roles: string[] = [];
  public user: string = '';
  public current_user: any;
  public user_is_admin: boolean = false;
  public returned_user: any;

  constructor(
    private tokenStorageService: TokenStorageService,
    private authService: AuthService,
    private router: Router,
  ) { }

  ngOnInit(): void {
    this.checkToken();
  }

  checkToken() {
    if (this.tokenStorageService?.getToken()) {
      this.isLoggedIn = true;
      this.roles = this.tokenStorageService?.getUser().roles;
      this.user = this.tokenStorageService?.getUser().username;
      this.current_user = this.tokenStorageService?.getUser();
      this.user_is_admin = this.roles.includes('ROLE_ADMIN');
      this.getUser(this.current_user.id)
    }
  }

  getUser(id) {
    this.authService.getUserById(id).subscribe(res => {
      this.returned_user = res;
      console.log('ssssss', res)
    })
  }

  logout(): void {
    this.tokenStorageService.signOut();
    this.isLoggedIn = false;
    const url = `/login`;
    this.router.navigate([url]);
  }

}
