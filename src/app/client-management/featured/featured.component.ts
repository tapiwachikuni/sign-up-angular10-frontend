
import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { TokenStorageService } from 'src/app/services/token-storage.service';

@Component({
  selector: 'app-featured',
  templateUrl: './featured.component.html',
  styleUrls: ['./featured.component.css']
})
export class FeaturedComponent implements OnInit {

  public isLoggedIn = false;
  public user: any;
  public current_user: any;
  public user_is_admin: boolean = false;

  constructor(
    private tokenStorageService: TokenStorageService,
    private authService: AuthService,
  ) { }

  ngOnInit(): void {
    this.getCurrentUser();
  }

  getCurrentUser() {
    this.user = this.tokenStorageService.getUser()
    if (this.user) {
      this.isLoggedIn = true;
      this.authService.getUserById(this.user.id).subscribe(res => {
        this.current_user = res;
      })
    }
  }

}
