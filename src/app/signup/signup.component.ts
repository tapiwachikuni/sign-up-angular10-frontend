import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from "@angular/forms";
import { HttpEvent, HttpEventType } from '@angular/common/http';
import { FileUploadService } from '../services/file-upload.service';
import { NotifierService } from 'src/app/services/notifier.service';
import { User } from '../models/user';
import { BehaviorSubject } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})

export class SignupComponent implements OnInit {
  public preview: string;
  public form: FormGroup;
  public percentDone: any = 0;
  public users = [];
  public user: User;

  public isSuccessful = false;
  public isSignUpFailed = false;
  public errorMessage = '';
  public countries = [];
  public lat;
  public lng;
  public position = new BehaviorSubject<any>([]);
  public loading: boolean = false;
  public upload_image_msg: string;

  constructor(
    public fb: FormBuilder,
    public fileUploadService: FileUploadService,
    private notifier: NotifierService,
    private router: Router,
  ) {

  }

  ngOnInit() {
    this.initializeForm();
    this.getLocation();
  }

  getLocation() {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition((position: Position) => {
        if (position) {
          this.lat = position.coords.latitude;
          this.lng = position.coords.longitude;
          this.position.next(position)
          this.initializeForm();
        }
      },
        (error: PositionError) => console.log(error));
    } else {
      alert("Geolocation is not supported by this browser.");
    }
  }

  initializeForm() {
    // Reactive Form
    let position = this.position.getValue()
    this.form = this.fb.group({
      first_name: [''],
      last_name: [''],
      username: [''],
      phone_number: [0],
      email: [''],
      avatar: [null],
      country_name: [''],
      status: [''],
      city: [''],
      latitude: position.coords?.latitude,
      longitude: position.coords?.longitude,
      password: [''],
    })
  }

  // Image Preview
  uploadFile(event) {
    const file = (event.target as HTMLInputElement).files[0];
    this.form.patchValue({
      avatar: file
    });
    this.form.get('avatar').updateValueAndValidity()

    // File Preview
    const reader = new FileReader();
    reader.onload = () => {
      this.preview = reader.result as string;
    }
    reader.readAsDataURL(file)
  }

  submitForm() {
    this.loading = true;
    this.upload_image_msg = ''
    this.fileUploadService.addUser(this.form.value
    ).subscribe((event: HttpEvent<any>) => {
      switch (event.type) {
        case HttpEventType.Sent:
          console.log('Request has been made!');
          break;
        case HttpEventType.ResponseHeader:
          console.log('Response header has been received!');
          break;
        case HttpEventType.UploadProgress:
          this.percentDone = Math.round(event.loaded / event.total * 100);
          console.log(`Uploaded! ${this.percentDone}%`);
          break;
        case HttpEventType.Response:
          console.log('User successfully created!', event.body);
          this.isSuccessful = true;
          this.isSignUpFailed = false;
          this.notifier.Notification("success", "successfully signed up");
          const url = `/login`;
          this.router.navigate([url]);
          this.percentDone = false;
          this.initializeForm();
          this.notifier.Notification("success", "User Successfully Created");
          this.loading = false;
          break;
        default:
          this.loading = false;
          this.upload_image_msg = 'Upload Profile Image'
          this.notifier.Notification("warning", "Failed To Create User");
          break;
      }
    })
  }


}


