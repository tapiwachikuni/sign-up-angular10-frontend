import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { HttpHeaders, HttpErrorResponse, HttpClient } from '@angular/common/http';
import { User } from '../models/user';

@Injectable({
    providedIn: 'root'
})

export class FileUploadService {

    baseURL = "http://localhost:3000/api/";

    headers = new HttpHeaders().set('Content-Type', 'application/json');

    constructor(private http: HttpClient) { }

    // Get Users
    getUsers() {
        return this.http.get<User[]>(this.baseURL + 'Users')
    }

    //Get User by ID 
    getUserById(id: string) {
        return this.http.get<User>(this.baseURL + 'Users' + '/' + id);
    }

    // Create User
    addUser(user: User): Observable<any> {
        var formData: any = new FormData();
        formData.append("first_name", user.first_name);
        formData.append("last_name", user.last_name);
        formData.append("username", user.username);
        formData.append("phone_number", user.phone_number);
        formData.append("email", user.email);
        formData.append("avatar", user.avatar);
        formData.append("country_name", user.country_name);
        formData.append("city", user.city);
        formData.append("latitude", user.latitude);
        formData.append("longitude", user.longitude);
        formData.append("password", user.password);

        return this.http.post<User>(this.baseURL + 'create-user', formData, {
            reportProgress: true,
            observe: 'events'
        })
    }

    // Delete user
    deleteUser(id: string) {
        return this.http.delete(this.baseURL + 'users' + '/' + id);
    }

    // Error handling 
    errorMgmt(error: HttpErrorResponse) {
        let errorMessage = '';
        if (error.error instanceof ErrorEvent) {
            // Get client-side error
            errorMessage = error.error.message;
        } else {
            // Get server-side error
            errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
        }
        console.log(errorMessage);
        return throwError(errorMessage);
    }

}